package com.talos.javatraining.lesson3;

import org.apache.commons.lang3.StringUtils;

import java.util.List;


public interface Animal
{

	default String getName()
	{
		return getClass().getSimpleName();
	}

	List<String> getCharacteristics();

	default String getFullDescription()
	{
		StringBuilder builder = new StringBuilder();
		String name = getName();
		String lastChr = name.substring(name.length()-1);
		builder.append(name).append(lastChr.equals("s") ? "es" : "s").append(" have these characteristics :");
		for (String characteristic : getCharacteristics())
		{
			builder.append(StringUtils.LF).append(StringUtils.CR).append("- ").append(characteristic);
		}
		return builder.toString();
	}

	static Object create(String className){
		final String PACKAGE = Animal.class.getPackage().getName() +".impl.animals.";
		if(className.equals(null)){
			return null;
		}
		try{
			Object c = Class.forName(PACKAGE+className).newInstance();
			if (c instanceof Animal){
				return c;
			}
			return null;
		}catch (ClassNotFoundException | IllegalAccessException | InstantiationException e){
			return null;
		}
	}

}
